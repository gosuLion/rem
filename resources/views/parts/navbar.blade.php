<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">REM</a>
        </div>
        <ul class="nav navbar-nav">
            <li class="active"><a href="/">Acasa</a></li>
            @if (!Auth::guest() && Auth::user())

            <li ><a href="/home">Cont</a></li>
            <li><a href="/electrica">Index Curent</a></li>
            {{--<li><a href="/apa">Index Apa</a></li>--}}
            <li><a href="/factura-curenta">Factura Curenta</a></li>
            <li><a class="dropdown-toggle" data-toggle="dropdown" href="/facturi-generale">Facturi Generale
                <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="/istoric-facturi">Isotric Facturi</a> </li>
                    @if (!Auth::guest() && Auth::user()->role == 'admin')
                        <li><a href="/adauga-factura-generala">Adauga factura generala</a> </li>
                        <li><a href="/adauga-utilitati">Adauga Utilitati</a> </li>
                    @endif

                </ul>

            </li>

            @endif
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <!-- Authentication Links -->
            @if (Auth::guest())
                <li><a href="{{ route('login') }}"><span class="fa fa-sign-in"></span> Login</a></li>
                <li><a href="{{ route('register') }}"><span class="fa fa-user"></span> Register</a></li>
            @else
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>
            @endif
        </ul>
    </div>
</nav>