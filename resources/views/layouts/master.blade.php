<html>
<head>
    <title>Rem Housekeeper - @yield('title')</title>
    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

</head>
<header>
<!-- Navigation -->
@include('parts.navbar')
<!-- END Navigation -->
</header>
<body>

<div class="container">
    @yield('content')
</div>
@include('parts.footerscripts')
</body>
</html>
