@extends('layouts.master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Meniu</div>

                <div class="list-group">
                        <a class="list-group-item" href="/electrica">Index Curent</a>
                        {{--<a class="list-group-item" href="/apa">Index Apa</a>--}}
                        <a class="list-group-item" href="/factura-curenta">Factura</a>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
