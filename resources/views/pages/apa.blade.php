@extends('layouts.master')

@section('content')
    <h2>{{ucfirst($path)}} Page</h2>
    <h3>{{$name}}</h3>

    @if (Session::has('message'))
        <div class="alert alert-success">{{ Session::get('message') }}</div>
    @endif
    @if (Session::has('warning'))
        <div class="alert alert-warning">{{ Session::get('warning') }}</div>
    @endif

<div class="col-xs-12 col-sm-8 col-sm-offset-2">
    <form class="form-inline" action="/{{$path}}/save" method="post">
        <div class="form-group">
            <label for="pwd">Index {{ucfirst($path)}}:</label>
            <input type="text" class="form-control" name="valoare" id="pwd">
        </div>
        <div class="checkbox">
            <label><input type="checkbox"> Trimit Index</label>
        </div>
        {{ csrf_field() }}
        <button type="submit" class="btn btn-default">Trimite</button>
    </form>
</div>

    <div class="col-xs-12 col-sm-8 col-sm-offset">
        <h3>Istoric Index</h3>
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Valoare Index Nou</th>
                <th>Valoare Index Vechi</th>
                <th>Data</th>
            </tr>
            </thead>
            <tbody>
            @php
                $i = 0;
                $j = 0;
            @endphp
            @foreach($values as $value)

                <tr>
                    <td>{{$value->valoare}}</td>

                    <td>
                        @if($i < count($values)-1)
                            {{$values[$i+1]->valoare}}
                        @endif
                    </td>
                    <td>
                        @if($i < count($values)-1)
                            {{$value->valoare - $values[$j+1]->valoare}}
                        @endif
                    </td>
                    <td>{{$value->created_at}}</td>
                </tr>
                @php
                    $i++;
                    $j++;
                @endphp
            @endforeach
            </tbody>
        </table>

    </div>
    @endsection