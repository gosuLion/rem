@extends('layouts.master')
@section('content')
    <h1>All your bills are here</h1>

    <table class="table table-hover">
        <tr>
            <td>Numar KW</td>
            <td>Valoare kw</td>
            <td>Index Curent</td>
            <td>Fosa</td>
            <td>Scara</td>
            <td>Altele</td>
            <td>Total</td>
            <td>Data</td>
        </tr>
        @foreach($values as $value)
        <tr>
            <td>{{$value->numar_kw}}</td>
            <td>{{$value->valoare_kw}}</td>
            <td>{{$value->valoare_index}}</td>
            <td>{{$value->valoare_apa}}</td>
            <td>{{$value->valoare_scara}}</td>
            <td>{{$value->valoare_altele}}</td>
            <td>{{$value->valoare_totala}}</td>
            <td>{{$value->created_at}}</td>
        </tr>
            @endforeach
    </table>
    @endsection