@extends('layouts.master')

@section('content')
    <h1>Utilitati</h1>
    @if (Session::has('message'))
        <div class="alert alert-success">{{ Session::get('message') }}</div>
    @endif
    @if (Session::has('warning'))
        <div class="alert alert-warning">{{ Session::get('warning') }}</div>
    @endif


    <div class="col-xs-12 col-sm-8 col-sm-offset-2">
        <form class="form-group" action="/adauga-utilitati/save" method="post">
            <div class="form-group">
                <label for="apa">Apa:</label>
                <input type="text" class="form-control" name="apa" id="apa">
            </div>
            <div class="form-group">
                <label for="gunoi">Gunoi:</label>
                <input type="text" class="form-control" name="gunoi" id="gunoi">
            </div>
            <div class="form-group">
                <label for="scara">Scara:</label>
                <input type="text" class="form-control" name="scara" id="scara">
            </div>
            <div class="form-group">
                <label for="altele">Altele:</label>
                <input type="text" class="form-control" name="altele" id="altele">
            </div>
            <div class="form-group">
                <label for="kw">Valoare KW:</label>
                <input type="text" class="form-control" name="valoare_kw" id="kw">
            </div>
            {{ csrf_field() }}
            <button type="submit" class="btn btn-default">Trimite</button>
        </form>
    </div>

    <div class="col-xs-12 col-sm-8 col-sm-offset">
        <h3>Istoric Index</h3>
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Apa</th>
                <th>Gunoi</th>
                <th>Scara</th>
                <th>Altele</th>
                <th>Valoare KW</th>
                <th>Data</th>
            </tr>
            </thead>
            <tbody>
            @foreach($values as $value)

                <tr>
                    <td>{{$value->apa}}</td>
                    <td>{{$value->gunoi}}</td>
                    <td>{{$value->scara}}</td>
                    <td>{{$value->altele}}</td>
                    <td>{{$value->valoare_kw}}</td>
                    <td>{{$value->created_at}}</td>

                </tr>

            @endforeach
            </tbody>
        </table>

    </div>
    @endsection