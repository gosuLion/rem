@extends('layouts.master')

@section('content')
    <h3>Ultima ta factura , {{ Auth::user()->name }}</h3>
<table class="table table-hover">
        <tr>
        <td>Numar KW</td>
        <td>Valoare kw</td>
        <td>Index Curent</td>
        <td>Fosa</td>
        <td>Scara</td>
        <td>Altele</td>
        <td>Total</td>
        <td>Data</td>
    </tr>
    @foreach($values as $value)
        @if($value->status == 'Neachitat')

        <tr>
        <td>{{$value->numar_kw}}</td>
        <td>{{$value->valoare_kw}}</td>
        <td>{{$value->valoare_index}}</td>
        <td>{{$value->valoare_apa}}</td>
        <td>{{$value->valoare_scara}}</td>
        <td>{{$value->valoare_altele}}</td>
        <td>{{$value->valoare_totala}}</td>
        <td>{{$value->created_at}}</td>
    </tr>
    @else
        <td colspan="8">Factura ta din data de:  {{$value->created_at}} in valoare de: {{$value->valoare_totala}} lei, este achitata !</td>
    @endif
    @endforeach

</table>
    @endsection