<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/electrica', 'ElectricaController@index');
Route::get('/apa', 'ElectricaController@index');
Route::post('/electrica/save', 'ElectricaController@registerIndex');
Route::post('/apa/save', 'ElectricaController@registerIndex');

Route::get('/adauga-utilitati', 'UtilitatiController@index');
Route::post('/adauga-utilitati/save', 'UtilitatiController@saveIndex');

Route::get('factura-curenta', 'FacturaController@index');
Route::get('istoric-facturi', 'FacturaController@displayBills');

Auth::routes();

Route::get('/home', 'HomeController@index');

