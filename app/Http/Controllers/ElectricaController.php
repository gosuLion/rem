<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ElectricaController extends Controller
{

//TODO Create "Factura"

    /**
     * Return the main view
     */
    public function index()
    {
        $currentPath= Route::getFacadeRoot()->current()->uri();
        $path = explode('/',$currentPath);
        $data = array(
            'name'    =>  'Adauga Index',
            'path'    =>   $path[0],
            'values'  =>   $this->getIndex($path[0]),

            );

        return view('pages.'.$path[0])->with($data);
    }


    /**
     * Register the index to the database
     * based on the userId
     */
    public function registerIndex(Request $request)
    {
        $currentPath= Route::getFacadeRoot()->current()->uri();
        $path = explode('/',$currentPath);


        //Get the user id
        $userId = Auth::id();

        $time = $this->getFirstRow($currentPath);

        //Get the request and insert the data into the table
        if($time != '')
        {
            $inittime = strtotime($time->created_at);
            $newformat = date("m", $inittime);

        }



        if($time == '')
        {
            DB::table($path[0])->insert([
                ['valoare' => $request['valoare'],
                    'user_id'   =>  $userId,
                ],

            ]);
            return Redirect::back()->with('message','Index adaugat cu succes !');

        }
        elseif ($newformat != Carbon::now()->month)
        {
            DB::table($path[0])->insert([
                ['valoare' => $request['valoare'],
                    'user_id'   =>  $userId,
                ],

            ]);
            return Redirect::back()->with('message','Index adaugat cu succes !');

        }
        else
        {
            return Redirect::back()->with('warning','Indexul a fost deja adaugat luna aceasta !');

        }


    }

    /**
     * Get the index for the current userId
     */
    public function getIndex($currentPath)
    {
        $path = explode('/',$currentPath);
        $userId = Auth::id();

        $values = DB::table($path[0])->where('user_id', $userId)->orderBy('created_at', 'desc')->get();

        return $values;
    }

    public function getFirstRow($currentPath)
    {
        $path = explode('/',$currentPath);
        $userId = Auth::id();

        $values = DB::table($path[0])->where('user_id', $userId)->orderBy('created_at', 'desc')->first();

        return $values;
    }
}
