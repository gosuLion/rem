<?php

namespace App\Http\Controllers;

use App\Bill;
use App\Utilitati;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class FacturaController extends Controller
{
    public function index()
    {
        $userId = Auth::id();

        if($this->checkBill($userId))
       {
            $this->generateBill($userId);
       }

       $bill = $this->displayBill($userId);

       $data = array(

         'values'   =>  $bill,

       );

        return view('pages.factura')->with($data);
    }

    /**
     * Generate the bill for the current user
     */
    public function generateBill($userId)
    {

        //Get electric values for current user
        $electrica = DB::table('electrica')->where('user_id' , $userId)->orderBy('created_at', 'desc')->limit(2)->get();
        if(count($electrica) == 0)
        {
            $valoare_kw = 0;
            $electricIndex = 0;
        }
        elseif (count($electrica) == 1)
        {
            $valoare_kw = $electrica[0]->valoare;
            $electricIndex = $electrica[0]->valoare;
        }
        else
        {
            $valoare_kw = $electrica[0]->valoare - $electrica[1]->valoare;
            $electricIndex = $electrica[0]->valoare;



        }

        //Get apa values for current user
        $apa = DB::table('apa')->where('user_id' , $userId)->orderBy('created_at', 'desc')->limit(2)->get();
        if(count($apa) == 0)
        {
            $valoare_mc = 0;
            $apaIndex = 0;
        }
        elseif (count($apa) == 1)
        {
            $valoare_mc = $apa[0]->valoare;
        }
        else
        {
            $valoare_mc = $apa[0]->valoare - $apa[1]->valoare;

        }

        //Generate the bill

        $utilitati = Utilitati::orderBy('created_at', 'desc')->first();

        //Generate the total value
        $total = $valoare_kw * $utilitati->valoare_kw + $utilitati->apa + $utilitati->gunoi + $utilitati->scara + $utilitati->altele;

        //Insert the Generated bill to the databes for the current user
        $bill = new Bill();

        $bill->user_id = $userId;
        $bill->numar_kw = $valoare_kw;
        $bill->valoare_kw = $utilitati->valoare_kw;
        $bill->valoare_index = $electricIndex;
        $bill->valoare_apa = $utilitati->apa;
        $bill->valoare_scara = $utilitati->scara;
        $bill->valoare_altele = $utilitati->altele;
        $bill->valoare_totala = $total;
        $bill->status = 'Neachitat';
        $bill->save();

    }

    /**
     * Check if the bill was already created
     */
    public function checkBill($userId)
    {
        $bill = Bill::where('user_id', $userId)->orderBy('created_at', 'desc')->first();

        if(count($bill) == 0 )
        {
            return true;

        }
        else if($bill->created_at->format("m") == Carbon::now()->month)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    /**
     * Get all Bill to be displayed
     */

    public function displayBill($userId)
    {
        $bill = Bill::where('user_id', $userId)->orderBy('created_at', 'desc')->get();
        return $bill;
    }

    /**
     * Display all Bills for the current user,
     * ordered by date.
     */

    public function displayBills()
    {
        $userId = Auth::id();
        $bills = Bill::where('user_id', $userId)->orderBy('created_at', 'desc')->get();
        $data = array(
          'values' => $bills
        );
        return view('pages.facturi')->with($data);
    }

}
