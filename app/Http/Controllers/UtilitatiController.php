<?php

namespace App\Http\Controllers;

use App\Utilitati;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class UtilitatiController extends Controller
{
    public function index()
    {

        $values = Utilitati::orderBy('created_at', 'desc')->get();


        $data = array(
            'values'  =>   $values,
        );
        return view('pages.utilitati')->with($data);
    }

    public function saveIndex(Request $request)
    {

        $utilitati = new Utilitati();

        $timp = Utilitati::orderBy('created_at', 'desc')->first();


        if($timp['created_at'] == '')
        {
            $utilitati->apa = $request->apa;
            $utilitati->gunoi = $request->gunoi;
            $utilitati->scara = $request->scara;
            $utilitati->altele = $request->altele;
            $utilitati->valoare_kw = $request->valoare_kw;
            $utilitati->save();
            return Redirect::back()->with('message','Utilitati adaugate cu succes !');

        }

        elseif ($timp['created_at']->format("m") != Carbon::now()->month)
        {
            $utilitati->apa = $request->apa;
            $utilitati->gunoi = $request->gunoi;
            $utilitati->scara = $request->scara;
            $utilitati->altele = $request->altele;
            $utilitati->valoare_kw = $request->valoare_kw;
            $utilitati->save();
            return Redirect::back()->with('message','Utilitati adaugate cu succes !');
        }
        else
        {
            return Redirect::back()->with('warning','Utilitatile au fost deja adaugate luna aceasta: '. date("F", mktime(0, 0, 0, Carbon::now()->month, 1)));

        }



    }

}
